# KodeInDocker - Theia
Entorno de Desarrollo basado en [Eclipse Theia IDE](https://theia-ide.org/) con Git y Docker integrado!


## Imágenes
| Imagen | Contenido |
|--------|-----------|
| registry.gitlab.com/kodeindocker/theia:base | Theia Base |
| registry.gitlab.com/kodeindocker/theia | Imagen con git y clonado automatico de respositorio |
| registry.gitlab.com/kodeindocker/theia:go | Imagen con golang, git y clonado automático |


## Versiones
| Componente | Versión |
|------------|---------|
| Node  | 20 |
| Go    | 1.23 |
| Theia | 1.56.0 |

## Variables de entorno
|variable| tipo | uso |
---------|------|-----|
| GIT_REPO | string | URL del repositorio git a clonar |
| GIT_BRANCH | string | Clonar una rama específica
| GIT_USERNAME | string | Usuario con permisos para clonar el proyecto|
| GIT_PASSWORD | string | Contraseña |
| GIT_PASSWORD_FILE | string | Lee la contraseña desde el archivo especificado |
| GIT_FULLNAME | string | Nombre completo del usuario, utilizado para `git.username`|
| GIT_EMAIL | string | Correo, utilizado para `git.email`|
| GIT_REPO2 | string | URL de un segundo repositorio (opcional) |
| GITLAB_CREATE_MR | bool | true indica que se debe crear un MR en gitlab al realizar un push en una rama nueva |
| HTTPS | bool | indica que theia se debe iniciar con SSL


## Ejemplos
### Iniciar entorno de desarrollo con node, clonando un repositorio 
```sh
docker run -d -p 3000:3000 \
	-e GIT_USERNAME=yourusername \
	-e GIT_PASSWORD=your_password \
	-e GIT_FULLNAME="John Doe" \
	-e GIT_EMAIL="john@doe.com" \
	-e GIT_REPO=https://repo-url \
	registry.gitlab.com/kodeindocker/theia
```


### Iniciar entorno de desarrollo con golang, clonando un repositorio

```sh
docker run -d -p 3000:3000 \
	-e GIT_USERNAME=yourusername \
	-e GIT_PASSWORD=your_password \
	-e GIT_FULLNAME="John Doe" \
	-e GIT_EMAIL="john@doe.com" \
	-e GIT_REPO=https://repo-url \
	registry.gitlab.com/kodeindocker/theia:go
```

### Iniciar entorno utilizando una contraseña almacenada en archivo

```sh
docker run -d -p 3000:3000 \
	-e GIT_USERNAME=yourusername \
	-e GIT_PASSWORD_FILE=/run/secrets/git \
	-e GIT_FULLNAME="John Doe" \
	-e GIT_EMAIL="john@doe.com" \
	-e GIT_REPO=https://repo-url \
	-v ./secrets/git:/run/secrets/git \
	registry.gitlab.com/kodeindocker/theia:go
```

