#!/bin/sh
docker exec -u root kodeindocker_dind sh -c " \
mkdir -p /sys/fs/cgroup/docker && \
mkdir -p /sys/fs/cgroup/cpuset/docker && \
mkdir -p /sys/fs/cgroup/devices/docker  && \
mkdir -p /sys/fs/cgroup/memory/docker && \
mkdir -p /sys/fs/cgroup/pids/docker && \
mkdir -p /sys/fs/cgroup/blkio/docker && \
mkdir -p /sys/fs/cgroup/hugetlb/docker && \
mkdir -p /sys/fs/cgroup/perf_event/docker && \
mkdir -p /sys/fs/cgroup/freezer/docker && \
mkdir -p /sys/fs/cgroup/cpu,cpuacct/docker && \
mkdir -p /sys/fs/cgroup/net_cls,net_prio/docker && \
mkdir -p /sys/fs/cgroup/rdma/docker && \
chown rootless -R /sys/fs/cgroup/docker/  && \
chmod a+rw /sys/fs/cgroup/docker/  && \
chown rootless -R /sys/fs/cgroup/cpuset/docker/  && \
chown rootless -R /sys/fs/cgroup/devices/docker/  && \
chown rootless -R /sys/fs/cgroup/memory/docker/  && \
chown rootless -R /sys/fs/cgroup/pids/docker/  && \
chown rootless -R /sys/fs/cgroup/blkio/docker  && \
chown rootless -R /sys/fs/cgroup/hugetlb/docker  && \
chown rootless -R /sys/fs/cgroup/perf_event/docker  && \
chown rootless -R /sys/fs/cgroup/cpu,cpuacct/docker  && \
chown rootless -R /sys/fs/cgroup/net_cls,net_prio/docker  && \
chown rootless -R /sys/fs/cgroup/rdma/docker  && \
chown rootless /sys/fs/cgroup/freezer/docker
"

#tail -f /dev/null
