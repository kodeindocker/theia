#!/bin/bash
echo username=$GIT_USERNAME

if [ "$GIT_PASSWORD_FILE" != "" ]
then
	password=$( cat $GIT_PASSWORD_FILE )
	echo password=$password;
else
	echo password=$GIT_PASSWORD;
fi

