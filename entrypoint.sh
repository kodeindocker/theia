#!/bin/bash
	echo "iniciando entorno"
	project_path=/home/project/$PROJECT_NAME

    PATH="$PATH:/sbin:/bin:/usr/bin:/usr/local/bin:/usr/sbin:/home/theia/go/bin/:/home/project/bin/"
	export PATH=$PATH
    echo "export PATH=$PATH" > /home/theia/.profile

	if [ "$GITLAB_CREATE_MR" = "true" ]; then
		git config --global --add push.pushOption merge_request.create
		git config --global --add push.pushOption merge_request.remove_source_branch
	fi

	if [ "$GIT_REPO" != "" ]; then
		project_path=$(echo "$GIT_REPO" | sed -e "s/^https:\/\///g")
		project_path=$(echo "$project_path" | sed -e "s/\.git$//g")
		project_path=/home/project/src/$project_path

		# descargo el reposiotrio
		echo -n "clonando $GIT_REPO "
		url=$GIT_REPO
		branch=""
		if [ "$GIT_BRANCH" != "" ]; then
			echo " - Rama $GIT_BRANCH - "
			git clone --branch "$GIT_BRANCH" "$url" $project_path
		else
			git clone "$url" $project_path
		fi
		echo "Ok"
		cd $project_path
		git submodule init
		git submodule update
	else
		cd /home/project/$PROJECT_NAME

		# obtengo el path desde el repositorio
		project_path=$(git remote get-url --all origin | sed -r 's/https[:/]+(.+@|)(.+)\.git$/\2/')
		project_path=$(echo $project_path | sed -r 's/git@(.+):(.+)\.git$/\1\/\2/')
		project_path=/home/project/src/$project_path
		mkdir -p $(dirname $project_path)
		echo $project_path
		if [ ! -d "$project_path" ]; then
			ln -s /home/project/$PROJECT_NAME $project_path
		fi
	fi

	if [ "$GIT_REPO2" != "" ]; then
		project_path2=$(echo "$GIT_REPO2" | sed -e "s/^https:\/\///g")
		project_path2=$(echo "$project_path2" | sed -e "s/\.git$//g")
		project_path2=/home/project/src/$project_path2

		# descargo el reposiotrio
		url=${GIT_REPO2/"https://"/"https://${credenciales}"}
		git clone "$url" $project_path2
	fi


	if [ ! -f "/home/theia/init_done" ]; then
		git config --global user.name "$GIT_FULLNAME"
		git config --global user.email "$GIT_EMAIL"

		if [ "$TASKS_INI" != "" ]; then
			ts=$(date)
			echo "************ [$ts] Ejecutando TASKS_INI ******************"
			# $TASKS_INI
			OLDIFS=$IFS;
			IFS=";";
			for t in $TASKS_INI; do
				echo "Ejecutando task: $t"
				sh -c "$t"
			done
			IFS=$OLDIFS
			echo "************  FIN  ******************"
		fi
	fi
	touch /home/theia/init_done

	if [ "$TASKS_COMMAND" != "" ]; then
		echo "************ [$ts] Ejecutando TASKS_COMMAND ******************"
		# $TASKS_COMMAND
		OLDIFS=$IFS;
		IFS=";";
		for t in $TASKS_COMMAND; do
			echo "Ejecutando task: $t"
			sh -c "$t"
		done
		IFS=$OLDIFS
		echo "************  FIN  ******************"
	fi

	cd /home/theia
	echo "Project Path: $project_path"

    # busco el workspace
	workspace=$(find $project_path -type f -name "*.theia-workspace" | head -n 1)

	if [ "$workspace" = "" ]; then
		workspace="$project_path"
	fi
	echo "workspace: $workspace"
	cd $project_path

	if [ "$HTTPS" = "true" ]; then
		mkdir -p /home/theia/certs
		if [ ! -f  "/home/theia/certs/server.crt" ]; then
			echo "Generando Certificado Autofirmado"
			openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout certs/server.key -out  certs/server.crt -batch
		fi
		node /home/theia/src-gen/backend/main.js "$workspace" --hostname=0.0.0.0 --ssl --cert /home/theia/certs/server.crt --certkey /home/theia/certs/server.key
	else 
		node /home/theia/src-gen/backend/main.js "$workspace" --hostname=0.0.0.0 
	fi



