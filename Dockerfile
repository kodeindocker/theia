ARG tag
FROM registry.gitlab.com/kodeindocker/theia:base$tag 
USER root
HEALTHCHECK --interval=10s --timeout=3s CMD curl -f http://localhost:3000 || exit 1
RUN apk add --no-cache git sudo docker docker-compose openssl git-subtree curl build-base nginx python3 && \
    echo '%wheel ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers && \
    addgroup theia wheel && \
    addgroup theia node && \
    addgroup theia docker; \
		mkdir -p /home/theia/.config && \
        mkdir -p /home/theia/node_modules/@theia/plugin-ext/lib/plugin/node/extensions/node_modules && \
        ln -s /home/theia/node_modules/typescript /home/theia/node_modules/@theia/plugin-ext/lib/plugin/node/extensions/node_modules && \
		chown theia /home/theia/.config -R && \
		chown theia /etc/nginx/ -R && \
		chown theia /var/lib/nginx -R && \
		chown theia /var/log/nginx/ -R && \
		chown theia -R /run/nginx && \
		npm install -g pnpm node-gyp
COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
USER theia
COPY git-helper.sh /home/theia/git-helper.sh
RUN git config --global credential.helper /home/theia/git-helper.sh


